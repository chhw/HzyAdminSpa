﻿using HZY.EFCore.Extensions;
using HZY.EFCore.Models;
using HZY.Infrastructure;
using HZY.Model.BO;
using HZY.Models.DTO;
using HZY.Models.Entities;
using HZY.Models.Entities.Framework;
using HZY.Repositories.Framework;
using HZY.Services.Accounts;
using HZY.Services.Admin.ServicesAdmin;
using HZY.Services.Consts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HZY.Services.Admin.Framework;

/// <summary>
/// 菜单服务
/// </summary>
public class SysMenuService : AdminBaseService<SysMenuRepository>
{
    private readonly SysFunctionRepository _sysFunctionRepository;
    private readonly SysMenuFunctionRepository _sysMenuFunctionRepository;
    private readonly SysRoleMenuFunctionRepository _sysRoleMenuFunctionRepository;
    private readonly AccountInfo _accountInfo;
    private readonly AppConfiguration _appConfiguration;

    public SysMenuService(SysMenuRepository repository,
        SysFunctionRepository sysFunctionRepository,
        SysMenuFunctionRepository sysMenuFunctionRepository,
        SysRoleMenuFunctionRepository sysRoleMenuFunctionRepository,
        IAccountService accountService,
        AppConfiguration appConfiguration) : base(
        repository)
    {
        _sysFunctionRepository = sysFunctionRepository;
        _sysMenuFunctionRepository = sysMenuFunctionRepository;
        _sysRoleMenuFunctionRepository = sysRoleMenuFunctionRepository;
        _appConfiguration = appConfiguration;
        this._accountInfo = accountService.GetAccountInfo();
    }

    /// <summary>
    /// 获取列表数据
    /// </summary>
    /// <param name="page"></param>
    /// <param name="size"></param>
    /// <param name="search"></param>
    /// <returns></returns>
    public async Task<PagingViewModel> FindListAsync(int page, int size, SysMenu search)
    {
        var query = (from sysMenu in this.Repository.Orm.SysMenu
                     from sysMenuParent in this.Repository.Orm.SysMenu.Where(w => w.Id == sysMenu.ParentId).DefaultIfEmpty()
                     select new { t1 = sysMenu, t2 = sysMenuParent })
              .WhereIf(search?.ParentId == 0 || search?.ParentId == null, w => w.t1.ParentId == null || w.t1.ParentId == 0)
              .WhereIf(search?.ParentId != 0 && search?.ParentId != null, w => w.t1.ParentId == search.ParentId)
              .WhereIf(!string.IsNullOrWhiteSpace(search?.Name), a => a.t1.Name.Contains(search.Name))
              .OrderBy(w => w.t1.Number)
              .Select(w => new
              {
                  w.t1.Id,
                  w.t1.Number,
                  w.t1.Name,
                  w.t1.Url,
                  父级菜单 = w.t2.Name,
                  w.t1.ComponentName,
                  w.t1.Router,
                  w.t1.Icon,
                  w.t1.Close,
                  w.t1.Show,
                  UpdateTime = w.t1.LastModificationTime.ToString("yyyy-MM-dd"),
                  CreateTime = w.t1.CreationTime.ToString("yyyy-MM-dd"),
              })
          ;

        return await this.Repository.AsPagingViewModelAsync(query, page, size);
    }

    /// <summary>
    /// 根据id数组删除
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    public async Task DeleteListAsync(List<int> ids)
    {
        foreach (var item in ids)
        {
            //删除当前菜单及一下的子集菜单
            var menu = await this.Repository.FindByIdAsync(item);
            var menus = await this.Repository.ToListAsync(w => w.LevelCode == menu.LevelCode || w.LevelCode.StartsWith(menu.LevelCode + "."));
            await this.Repository.DeleteAsync(menus);
            //删除菜单关联表
            await this._sysRoleMenuFunctionRepository.DeleteAsync(w => menus.Select(w => w.Id).Contains(w.MenuId));
            await this._sysMenuFunctionRepository.DeleteAsync(w => menus.Select(w => w.Id).Contains(w.MenuId));
        }
    }

    /// <summary>
    /// 查询表单数据
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<Dictionary<string, object>> FindFormAsync(int id)
    {
        var res = new Dictionary<string, object>();

        var form = await this.Repository.FindByIdAsync(id);
        var allFunctions = await this._sysFunctionRepository.Select
            .OrderBy(w => w.Number)
            .ToListAsync();
        var functionIds = await this._sysMenuFunctionRepository.Select
            .Where(w => w.MenuId == id)
            .Select(w => w.FunctionId)
            .ToListAsync();

        res[nameof(id)] = id == 0 ? "" : id;
        res[nameof(form)] = form.NullSafe();
        res[nameof(allFunctions)] = allFunctions;
        res[nameof(functionIds)] = functionIds;
        return res;
    }

    /// <summary>
    /// 保存数据
    /// </summary>
    /// <param name="form"></param>
    /// <returns></returns>
    public async Task<SysMenu> SaveFormAsync(SysMenuFormDto form)
    {
        var model = form.Form;
        var functionIds = form.FunctionIds;

        model = await this.Repository.InsertOrUpdateAsync(model);

        #region 更新级别码

        if (model.ParentId == null || model.ParentId == 0)
        {
            model.LevelCode = model.Id.ToString();
        }
        else
        {
            var parent = await this.Repository.FindByIdAsync(model.ParentId);
            model.LevelCode = parent.LevelCode + "." + model.Id;
        }

        model = await this.Repository.InsertOrUpdateAsync(model);

        #endregion

        #region 处理菜单功能绑定表

        await this._sysMenuFunctionRepository.DeleteAsync(w => w.MenuId == model.Id);
        if (functionIds.Count <= 0) return model;

        var sysMenuFunctionList = await this._sysMenuFunctionRepository.Select
            .Where(w => w.MenuId == model.Id)
            .ToListAsync();

        foreach (var item in functionIds)
        {
            var sysMenuFunction = sysMenuFunctionList.FirstOrDefault(w => w.FunctionId == item) ?? new SysMenuFunction();
            sysMenuFunction.Id = sysMenuFunction.Id == Guid.Empty ? Guid.Empty : sysMenuFunction.Id;
            sysMenuFunction.FunctionId = item;
            sysMenuFunction.MenuId = model.Id;
            await this._sysMenuFunctionRepository.InsertAsync(sysMenuFunction);
        }

        #endregion

        return model;
    }

    /// <summary>
    /// 导出Excel
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    public async Task<byte[]> ExportExcelAsync(SysMenu search)
    {
        var tableViewModel = await this.FindListAsync(1, 999999, search);
        return this.ExportExcelByPagingViewModel(tableViewModel, null, "Id");
    }

    /// <summary>
    /// 获取所有的菜单
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    public async Task<List<SysMenuTreeDto>> GetAllAsync(SysMenu search)
    {
        var query = (from sysMenu in this.Repository.Orm.SysMenu
                     from sysMenuParent in this.Repository.Orm.SysMenu.Where(w => w.Id == sysMenu.ParentId).DefaultIfEmpty()
                     select new { t1 = sysMenu, t2 = sysMenuParent })
             .WhereIf(!string.IsNullOrWhiteSpace(search?.Name), a => a.t1.Name.Contains(search.Name))
             .OrderBy(w => w.t1.Number)
             .Select(w => new SysMenuTreeDto
             {
                 Id = w.t1.Id,
                 Number = w.t1.Number,
                 Name = w.t1.Name,
                 Url = w.t1.Url,
                 ParentName = w.t2.Name,
                 ParentId = w.t1.ParentId,
                 JumpUrl = w.t1.JumpUrl,
                 ComponentName = w.t1.ComponentName,
                 Router = w.t1.Router,
                 Icon = w.t1.Icon,
                 Close = w.t1.Close,
                 Show = w.t1.Show,
                 KeepAlive = w.t1.KeepAlive,
                 State = w.t1.State,
                 LevelCode = w.t1.LevelCode,
                 LastModificationTime = w.t1.LastModificationTime,
                 CreationTime = w.t1.CreationTime,
             })
        ;

        return await query.ToListAsync();
    }

    #region 创建系统左侧菜单

    /// <summary>
    /// 根据角色ID 获取菜单
    /// </summary>
    /// <returns></returns>
    public async Task<List<SysMenu>> GetMenusByCurrentRoleAsync()
    {
        var sysMenuAllList = await this.Repository.Select
            .Where(w => w.State)
            .OrderBy(w => w.Number)
            .ToListAsync();

        if (this._accountInfo.IsAdministrator) return sysMenuAllList;

        var sysMenuList = await (
            from t1 in this.Repository.Orm.SysRoleMenuFunction.Where(w => this._accountInfo.SysRoles.Select(s => s.Id).Contains(w.RoleId))
            from t2 in this.Repository.Orm.SysFunction.Where(w => w.Id == t1.FunctionId && w.ByName == AdminFunctionConsts.Function_Display).DefaultIfEmpty()
            from t3 in this.Repository.Orm.SysMenu.Where(w => w.Id == t1.MenuId && w.State).DefaultIfEmpty()
            select t3
            )
            .ToListAsync()
            ;

        var newSysMenuList = new List<SysMenu>();

        foreach (var item in sysMenuList)
        {
            this.CheckUpperLevel(sysMenuAllList, sysMenuList, newSysMenuList, item);
            var item1 = item;
            if (newSysMenuList.Find(w => w.Id == item1.Id) == null)
                newSysMenuList.Add(item);
        }

        return newSysMenuList.OrderBy(w => w.Number).ToList();
    }

    private void CheckUpperLevel(List<SysMenu> sysMenuAllList, List<SysMenu> oldSysMenuList,
        List<SysMenu> newSysMenuList, SysMenu menu)
    {
        if (oldSysMenuList.Any(w => w.Id == menu.ParentId)) return;

        var item = sysMenuAllList.Find(w => w.Id == menu.ParentId);
        if (item == null) return;

        newSysMenuList.Add(item);

        this.CheckUpperLevel(sysMenuAllList, oldSysMenuList, newSysMenuList, item);
    }

    /// <summary>
    /// 创建菜单
    /// </summary>
    /// <param name="id"></param>
    /// <param name="sysMenuList"></param>
    public List<SysMenuTreeDto> CreateMenus(int id, List<SysMenu> sysMenuList)
    {
        var menus = id == 0
            ? sysMenuList.Where(w => w.ParentId == null || w.ParentId == 0).ToList()
            : sysMenuList.Where(w => w.ParentId == id).ToList();

        return menus.Select(item => new SysMenuTreeDto
        {
            Id = item.Id,
            Name = item.Name,
            ComponentName = item.ComponentName,
            Url = item.Url,
            Router = item.Router,
            JumpUrl = string.IsNullOrWhiteSpace(item.JumpUrl) ? item.Router : item.JumpUrl,
            Icon = item.Icon,
            Close = item.Close,
            Show = item.Show,
            KeepAlive = item.KeepAlive,
            State = item.State,
            ParentId = item.ParentId,
            LevelCode = item.LevelCode,
            Children = this.CreateMenus(item.Id, sysMenuList)
        }).ToList();
    }

    /// <summary>
    /// 获取拥有的菜单对象的权限
    /// </summary>
    /// <param name="sysMenuList"></param>
    /// <returns></returns>
    public async Task<List<Dictionary<string, object>>> GetPowerByMenusAsync(List<SysMenu> sysMenuList)
    {
        var sysFunctionList = await this._sysFunctionRepository.Select.OrderBy(w => w.Number).ToListAsync();
        var sysMenuFunctionList = await this._sysMenuFunctionRepository.Select.ToListAsync();
        var sysRoleMenuFunctionList = await this._sysRoleMenuFunctionRepository.Select
            .Where(w => this._accountInfo.SysRoles.Select(w => w.Id).Contains(w.RoleId))
            .ToListAsync();

        var res = new List<Dictionary<string, object>>();

        if (this._accountInfo.IsAdministrator)
        {
            foreach (var item in sysMenuList)
            {
                var power = new Dictionary<string, object>
                {
                    ["MenuId"] = item.Id
                };
                foreach (var sysFunction in sysFunctionList)
                {
                    if (string.IsNullOrWhiteSpace(sysFunction.ByName)) continue;

                    var isPower = sysMenuFunctionList
                        .Any(w => w.MenuId == item.Id && w.FunctionId == sysFunction.Id);
                    if (sysFunction.ByName == AdminFunctionConsts.Function_Display || item.ParentId == this._appConfiguration.SysMenuId)
                        isPower = true;
                    power.Add(sysFunction.ByName, isPower);
                }

                res.Add(power);
            }

            return res;
        }

        foreach (var item in sysMenuList)
        {
            var power = new Dictionary<string, object>
            {
                ["MenuId"] = item.Id
            };
            foreach (var sysFunction in sysFunctionList)
            {
                if (string.IsNullOrWhiteSpace(sysFunction.ByName)) continue;

                if (_accountInfo.SysRoles.Select(w => w.Id).Any())
                {
                    var isPower = sysRoleMenuFunctionList
                        .Any(w =>
                            this._accountInfo.SysRoles.Select(w => w.Id).Contains(w.RoleId) && w.MenuId == item.Id &&
                            w.FunctionId == sysFunction.Id);
                    power.Add(sysFunction.ByName, isPower);
                }
                else
                {
                    power.Add(sysFunction.ByName, false);
                }
            }

            res.Add(power);
        }

        return res;
    }

    /// <summary>
    /// 根据菜单获取权限
    /// </summary>
    /// <param name="menuId"></param>
    /// <returns></returns>
    public async Task<Dictionary<string, bool>> GetPowerStateByMenuId(int menuId)
    {
        var sysMenu = await this.Repository.FindByIdAsync(menuId);
        var sysFunctionList = await this._sysFunctionRepository.Select.OrderBy(w => w.Number).ToListAsync();
        var sysMenuFunctionList = await this._sysMenuFunctionRepository.Select.ToListAsync();
        var sysRoleMenuFunctionList = await this._sysRoleMenuFunctionRepository.Select
            .Where(w => this._accountInfo.SysRoles.Select(w => w.Id).Contains(w.RoleId))
            .ToListAsync();

        var power = new Dictionary<string, bool>();

        if (this._accountInfo.IsAdministrator)
        {
            foreach (var item in sysFunctionList)
            {
                if (string.IsNullOrWhiteSpace(item.ByName)) continue;

                var isPower = sysMenuFunctionList.Any(w => w.MenuId == menuId && w.FunctionId == item.Id);
                if (item.ByName == AdminFunctionConsts.Function_Display || sysMenu.ParentId == this._appConfiguration.SysMenuId)
                    isPower = true;
                power.Add(item.ByName, isPower);
            }

            return power;
        }

        foreach (var item in sysFunctionList)
        {
            if (string.IsNullOrWhiteSpace(item.ByName)) continue;

            if (_accountInfo.SysRoles.Select(w => w.Id).Any())
            {
                var isPower = sysRoleMenuFunctionList
                    .Any(w => this._accountInfo.SysRoles.Select(w => w.Id).Contains(w.RoleId) && w.MenuId == menuId &&
                              w.FunctionId == item.Id);
                power.Add(item.ByName, isPower);
            }
            else
            {
                power.Add(item.ByName, false);
            }
        }

        return power;
    }

    #endregion 左侧菜单

}