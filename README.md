# HzyAdminSpa

####  文档: https://gitee.com/hzy6/HzyAdminSpa/wikis/pages


### 后台模板
https://gitee.com/hzy6/hzy-admin-spa-ui

### WebApi 任务调度平台
https://gitee.com/hzy6/hzy-quartz

### 🚩  代码生成器 

https://gitee.com/hzy6/hzy-admin-code-generation

### Java 后台框架

1. https://gitee.com/hzy6/hzy-admin

2. https://gitee.com/hzy6/hzy-admin-spa

### 目录结构
![输入图片说明](https://images.gitee.com/uploads/images/2021/1111/110155_2e2c13f4_1242080.png "屏幕截图.png")

#### 📝  介绍 
前后分离,后台通配权限管理系统！

    数据库脚本位置(默认 PostgreSql 数据库)：

        PostgreSql > 根目录/doc/HzyAdminSpa_PostgreSql.sql

        SqlServer > 根目录/doc/HzyAdminSpa.sql

        MySql > 根目录/doc/hzyadminspa_MySql.sql


    后端技术：.Net6、EFCore6、Swagger
    
    前端：Vue3.x 、Antd Of Vue 3.0
    
    
    
    #### 软件架构
    开发环境：vs2022 、 .Net6.0 、VsCode
    
    ###  **_交流群: 534584927_** 



#### ✨  特技 

| ![输入图片说明](gitee/images/image.png) | ![输入图片说明](gitee/images/icons.png) |
|-----------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------|
| ![输入图片说明](gitee/images/chart.png) | ![输入图片说明](gitee/images/%E6%9B%B4%E5%A4%9A%E5%9B%BE%E8%A1%A8.png) |
| ![输入图片说明](gitee/images/user_list.png) | ![输入图片说明](gitee/images/wangeditor.png) |
| ![输入图片说明](gitee/images/api_doc.png) | ![输入图片说明](gitee/images/menu_page.png) |

![输入图片说明](gitee/images/user_center.png)
![输入图片说明](gitee/images/login.png)


#### 安装前提

前端 ui 参考地址：https://gitee.com/hzy6/hzy-admin-spa-ui

1、安装 nodejs

2、安装 Vite

#### 安装教程

1. 前端 UI 在项目跟目录下 hzy-admin-clientapp 使用 VS Code 打开
2. VS Code 打开终端执行CMD命令>> cnpm install 拉包 （node 环境 这些不懂得自行百度查询资料！）
3. 然后使用 Vs 2019 打开服务端代码 f5 调试模式 运行即可
注意：![输入图片说明](https://images.gitee.com/uploads/images/2019/1224/131124_8c2c3463_1242080.png "屏幕截图.png")

请使用这种独立模式启动，不要使用 iis 模式


4、命令 npm run build 打包后使用 iis 或者 nginx 部署前端 ui



#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


